package com.siddhi.virtualpower.battery.usecase;

import com.siddhi.virtualpower.battery.domain.Battery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class GenerateBatterySummaryUseCaseTest {

    @InjectMocks
    private GenerateBatterySummaryUseCase generateBatterySummaryUseCase;

    @Test
    void givenListOfBattery_whenExecute_thenReturnBatterSummary(){
        List<Battery> batteries = prepareDummyList();

        List<String> allNames = batteries.stream()
                .map(Battery::getName)
                .toList();

        var batterySummary = generateBatterySummaryUseCase.execute(batteries);

        Assertions.assertEquals(1500,batterySummary.getTotalWatts());
        Assertions.assertEquals(300,batterySummary.getAverageWatt());

        var sortedNames = allNames.stream().sorted().toList();

        Assertions.assertEquals(sortedNames,batterySummary.getBatteryNames());

    }

    private List<Battery> prepareDummyList() {
        Battery battery1 = new Battery("battery1","121",100);
        Battery battery2 = new Battery("battery2","122",200);
        Battery battery3 = new Battery("battery3","123",300);
        Battery battery4 = new Battery("battery4","124",400);
        Battery battery5 = new Battery("battery5","125",500);

        return List.of(battery5,battery4,battery3,battery2,battery1);
    }

}