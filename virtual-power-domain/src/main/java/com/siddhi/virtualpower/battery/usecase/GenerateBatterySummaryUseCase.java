package com.siddhi.virtualpower.battery.usecase;

import com.siddhi.virtualpower.battery.domain.Battery;
import com.siddhi.virtualpower.battery.domain.BatterySummary;

import java.util.List;

public class GenerateBatterySummaryUseCase {

    public BatterySummary execute(List<Battery> matchedBatteries) {
        List<String> batteryNames = matchedBatteries.stream().map(Battery::getName).sorted().toList();
        long totalWatts = matchedBatteries.isEmpty() ? 0 : matchedBatteries.stream().mapToInt(Battery::getWattCapacity).sum();
        double averageWatts = matchedBatteries.isEmpty() ? 0 : ((double) totalWatts) / matchedBatteries.size();
        return BatterySummary
                .builder()
                .batteryNames(batteryNames)
                .totalWatts(totalWatts)
                .averageWatt(averageWatts)
                .build();
    }
}
