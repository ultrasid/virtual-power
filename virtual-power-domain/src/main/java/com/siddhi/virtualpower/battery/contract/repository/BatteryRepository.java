package com.siddhi.virtualpower.battery.contract.repository;


import com.siddhi.virtualpower.battery.domain.Battery;

import java.util.List;

public interface BatteryRepository {

    List<Battery> saveAll(List<Battery> batteries);

    Boolean existsByName(String name);

}
