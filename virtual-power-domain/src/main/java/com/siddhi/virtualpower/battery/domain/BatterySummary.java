package com.siddhi.virtualpower.battery.domain;

import lombok.Builder;
import lombok.Getter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Getter
@Builder
public class BatterySummary implements Serializable {
    @Serial
    private static final long serialVersionUID = 3052432096986245108L;

    List<String> batteryNames;
    private long totalWatts;
    private double averageWatt;
}
