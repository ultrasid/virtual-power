package com.siddhi.virtualpower.battery.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Battery implements Serializable {

    @Serial
    private static final long serialVersionUID = -5019681341505283303L;

    private String name;
    private String postCode;
    private int wattCapacity;

}