package com.siddhi.virtualpower.battery.service;

import com.siddhi.virtualpower.battery.contract.repository.BatteryRepository;
import com.siddhi.virtualpower.battery.exception.InvalidBatteryRequestException;
import com.siddhi.virtualpower.battery.mapper.BatteryMapper;
import com.siddhi.virtualpower.battery.mapper.BatterySummaryMapper;
import com.siddhi.virtualpower.battery.model.BatteryRequest;
import com.siddhi.virtualpower.battery.repository.BatteryQueryRepository;
import com.siddhi.virtualpower.battery.resource.BatteryResource;
import com.siddhi.virtualpower.battery.resource.BatterySearchResource;
import com.siddhi.virtualpower.battery.usecase.GenerateBatterySummaryUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class BatteryServiceImpl implements BatteryService {

    private final BatteryRepository repository;
    private final BatteryQueryRepository batteryQueryRepository;
    private final GenerateBatterySummaryUseCase generateBatterySummaryUseCase;
    private final BatterySummaryMapper batterySummaryMapper;
    private final BatteryMapper batteryMapper;

    @Override
    public List<BatteryResource> saveAll(List<BatteryRequest> batteryRequests) {
        log.debug("Received a request to save a list of batteries.");
        List<String> allErrors = IntStream.range(0, batteryRequests.size())
                .mapToObj(index -> validateBatteryRequest(batteryRequests.get(index), index))
                .flatMap(List::stream).toList();

        if (!allErrors.isEmpty()) {
            log.warn("Invalid Battery Information. Errors: {}", allErrors);
            throw new InvalidBatteryRequestException(allErrors, "Invalid Battery Information");
        }

        List<BatteryRequest> distinctBatteryRequests = batteryRequests.stream()
                .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BatteryRequest::getName)))).stream().toList();

        List<BatteryResource> savedBatteries = batteryMapper.toResource(repository.saveAll(batteryMapper.toDomain(distinctBatteryRequests)));
        log.debug("Successfully saved {} batteries.", savedBatteries.size());
        return savedBatteries;
    }

    @Override
    public BatterySearchResource searchForBatteries(List<String> postCodes) {
        log.debug("Received a search request for post codes: {}", postCodes);
        var matchedBatteries = batteryQueryRepository.findByPostCodes(postCodes);
        BatterySearchResource searchResource = batterySummaryMapper.toResource(generateBatterySummaryUseCase.execute(matchedBatteries));
        log.debug("Search completed with {} batteries found.", searchResource.getBatteryNames().size());
        return searchResource;
    }

    @Override
    public Boolean existsByName(String name) {
        log.debug("Checking if a battery exists with the name: {}", name);
        return repository.existsByName(name);
    }

    private List<String> validateBatteryRequest(BatteryRequest batteryRequest, int index) {
        List<String> errors = new ArrayList<>();

        if (StringUtils.isEmpty(batteryRequest.getName())) {
            errors.add("Name is missing for battery request at position " + index);
        }

        if (StringUtils.isEmpty(batteryRequest.getPostCode())) {
            errors.add("Post Code is missing for battery request at position " + index);
        }

        if (batteryRequest.getWattCapacity() <= 0) {
            errors.add("Watt is invalid at position " + index);
        }

        if (StringUtils.isNotEmpty(batteryRequest.getPostCode()) &&
                StringUtils.isNotEmpty(batteryRequest.getName()) &&
                existsByName(batteryRequest.getName())
        ) {
            errors.add("Name: " + batteryRequest.getName() + " already exists");
        }

        return errors;
    }
}
