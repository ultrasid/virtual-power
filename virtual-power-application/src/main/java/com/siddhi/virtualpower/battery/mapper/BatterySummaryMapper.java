package com.siddhi.virtualpower.battery.mapper;


import com.siddhi.virtualpower.battery.domain.BatterySummary;
import com.siddhi.virtualpower.battery.resource.BatterySearchResource;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BatterySummaryMapper {
    BatterySearchResource toResource(BatterySummary batterySummary);
}
