package com.siddhi.virtualpower.battery.service;

import com.siddhi.virtualpower.battery.model.BatteryRequest;
import com.siddhi.virtualpower.battery.resource.BatteryResource;
import com.siddhi.virtualpower.battery.resource.BatterySearchResource;

import java.util.List;

public interface BatteryService {

    List<BatteryResource> saveAll(List<BatteryRequest> batteryRequests);

    BatterySearchResource searchForBatteries(List<String> postCodes);

    Boolean existsByName(String name);
}
