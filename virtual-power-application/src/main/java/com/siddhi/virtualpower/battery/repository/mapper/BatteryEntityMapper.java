package com.siddhi.virtualpower.battery.repository.mapper;

import com.siddhi.virtualpower.battery.domain.Battery;
import com.siddhi.virtualpower.battery.repository.entity.BatteryEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BatteryEntityMapper {

    Battery toDomain(BatteryEntity batteryEntity);

    BatteryEntity toEntity(Battery Battery);

    List<Battery> toDomain(List<BatteryEntity> batteryEntity);

    List<BatteryEntity> toEntity(List<Battery> Battery);

}
