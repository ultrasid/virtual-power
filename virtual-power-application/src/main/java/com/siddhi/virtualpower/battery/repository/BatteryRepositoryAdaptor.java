package com.siddhi.virtualpower.battery.repository;

import com.siddhi.virtualpower.battery.contract.repository.BatteryRepository;
import com.siddhi.virtualpower.battery.domain.Battery;
import com.siddhi.virtualpower.battery.repository.mapper.BatteryEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BatteryRepositoryAdaptor implements BatteryRepository, BatteryQueryRepository {

    private final BatteryJpaRepository repository;
    private final BatteryEntityMapper batteryEntityMapper;

    @Override
    public List<Battery> saveAll(List<Battery> batteries) {
        log.debug("Saving a list of batteries to the database.");
        var entities = repository.saveAll(batteryEntityMapper.toEntity(batteries));
        log.debug("Successfully saved {} batteries.", entities.size());
        return batteryEntityMapper.toDomain(entities);
    }

    @Override
    public Boolean existsByName(String name) {
        log.debug("Checking if a battery exists with the name: {}", name);
        return repository.existsByName(name);
    }

    @Override
    public List<Battery> findByPostCodes(List<String> postCodes) {
        log.debug("Finding batteries by post codes: {}", postCodes);
        List<Battery> batteries = repository.findAllByPostCodeIn(postCodes)
                .stream()
                .map(batteryEntityMapper::toDomain
                ).toList();
        log.debug("Found {} batteries with post codes: {}", batteries.size(), postCodes);
        return batteries;
    }
}
