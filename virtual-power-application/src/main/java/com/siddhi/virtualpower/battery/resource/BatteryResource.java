package com.siddhi.virtualpower.battery.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BatteryResource {
    private String name;
    private String postCode;
    private int wattCapacity;
}
