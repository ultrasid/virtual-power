package com.siddhi.virtualpower.battery.controller;

import com.siddhi.virtualpower.battery.model.BatteryRequest;
import com.siddhi.virtualpower.battery.resource.BatteryResource;
import com.siddhi.virtualpower.battery.resource.BatterySearchResource;
import com.siddhi.virtualpower.battery.service.BatteryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/batteries")
@RequiredArgsConstructor
public class BatteryController {

    private final BatteryService batteryService;

    @PostMapping
    public List<BatteryResource> addBatteries(@RequestBody List<BatteryRequest> batteryRequests) {
        log.debug("Received a request to add batteries. Number of batteries: {}", batteryRequests.size());
        return batteryService.saveAll(batteryRequests);
    }

    @GetMapping
    public BatterySearchResource searchForBatteries(@RequestParam List<String> postCodes) {
        log.debug("Received a search request for post codes: {}", postCodes);
        return batteryService.searchForBatteries(postCodes);
    }

}
