package com.siddhi.virtualpower.battery.config;

import com.siddhi.virtualpower.battery.usecase.GenerateBatterySummaryUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BatteryConfig {
    @Bean
    public GenerateBatterySummaryUseCase generateBatterySummaryUseCase(){
        return new GenerateBatterySummaryUseCase();
    }
}
