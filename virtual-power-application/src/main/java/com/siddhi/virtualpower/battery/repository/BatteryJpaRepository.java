package com.siddhi.virtualpower.battery.repository;

import com.siddhi.virtualpower.battery.repository.entity.BatteryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BatteryJpaRepository extends JpaRepository<BatteryEntity, Long>, JpaSpecificationExecutor<BatteryEntity> {

    List<BatteryEntity> findAllByPostCodeIn(List<String> postCodes);

    Boolean existsByName(String name);

}
