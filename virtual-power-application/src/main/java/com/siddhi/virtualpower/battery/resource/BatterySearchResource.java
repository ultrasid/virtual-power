package com.siddhi.virtualpower.battery.resource;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class BatterySearchResource {
    List<String> batteryNames;
    private long totalWatts;
    private double averageWatt;
}
