package com.siddhi.virtualpower.battery.exception;

import java.io.Serial;
import java.util.List;

public class InvalidBatteryRequestException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = -3430899010145872013L;
    private final List<String> errors;

    public InvalidBatteryRequestException(List<String> errors, String message) {
        super(message);
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }
}
