package com.siddhi.virtualpower.battery.mapper;


import com.siddhi.virtualpower.battery.domain.Battery;
import com.siddhi.virtualpower.battery.model.BatteryRequest;
import com.siddhi.virtualpower.battery.resource.BatteryResource;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BatteryMapper {
    BatteryResource toResource(Battery battery);
    List<BatteryResource> toResource(List<Battery> batteries);

    Battery toDomain(BatteryRequest batteryRequest);

    List<Battery> toDomain(List<BatteryRequest> batteryRequests);
}
