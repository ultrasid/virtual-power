package com.siddhi.virtualpower.battery.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Slf4j
public class BatteryExceptionHandler {
    @ExceptionHandler(InvalidBatteryRequestException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseEntity<Object> handleUnProcessableEntityException(InvalidBatteryRequestException invalidBatteryRequestException) {
        log.error(invalidBatteryRequestException.getMessage(), invalidBatteryRequestException);
        return ResponseEntity.unprocessableEntity().body(invalidBatteryRequestException.getErrors());
    }
}