package com.siddhi.virtualpower.battery.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatteryRequest {
    private String name;
    private String postCode;
    private int wattCapacity;
}
