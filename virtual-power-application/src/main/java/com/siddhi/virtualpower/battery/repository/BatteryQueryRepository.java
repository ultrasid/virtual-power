package com.siddhi.virtualpower.battery.repository;

import com.siddhi.virtualpower.battery.domain.Battery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BatteryQueryRepository {

    List<Battery> findByPostCodes(List<String> postCodes);
}
