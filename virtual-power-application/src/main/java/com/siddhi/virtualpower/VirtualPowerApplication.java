package com.siddhi.virtualpower;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VirtualPowerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VirtualPowerApplication.class, args);
	}

	@Bean
	public OpenAPI usersMicroserviceOpenAPI() {
		return new OpenAPI()
				.info(new Info().title("Virtual Power API Documentation")
						.description("Bulk Save and Retrieve using PostCode API")
						.version("1.0"));
	}

}
