package com.siddhi.virtualpower.battery.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.siddhi.virtualpower.battery.mapper.BatteryMapperImpl;
import com.siddhi.virtualpower.battery.model.BatteryRequest;
import com.siddhi.virtualpower.battery.resource.BatteryResource;
import com.siddhi.virtualpower.battery.resource.BatterySearchResource;
import com.siddhi.virtualpower.battery.service.BatteryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = BatteryController.class)
@ContextConfiguration(classes = {BatteryController.class, BatteryService.class, BatteryMapperImpl.class})
@AutoConfigureMockMvc(addFilters = false)
class BatteryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BatteryService batteryService;

    @Test
    void givenBulkBatteriesRequest_whenSendToSave_ShouldReturnBatteryResource() throws Exception {

        var batteryRequests = buildBatteryRequestList();

        var batteryResources = buildBatteryResourcetList();

        when(batteryService.saveAll(any())).thenReturn(batteryResources);

        mockMvc.perform(post("/api/v1/batteries")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(batteryRequests)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(batteryResources.get(0).getName())))
                .andExpect(jsonPath("$[0].postCode", is(batteryResources.get(0).getPostCode())))
                .andExpect(jsonPath("$[0].wattCapacity", is(batteryResources.get(0).getWattCapacity())));

    }

    @Test
    public void givenValidPostCodes_whenSearched_shouldReturnBatterySummariesMatchingPostCodes() throws Exception {
        List<String> postCodes = Arrays.asList("121", "122");

        BatterySearchResource expectedResponse = BatterySearchResource.builder()
                .batteryNames(List.of("Battery1", "Battery2")).averageWatt(100).totalWatts(200).build();
        when(batteryService.searchForBatteries(postCodes)).thenReturn(expectedResponse);

        mockMvc.perform(get("/api/v1/batteries")
                        .param("postCodes", postCodes.toArray(new String[0])))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedResponse)));
    }

    private BatteryRequest buildBatteryRequest() {
        BatteryRequest batteryRequest = new BatteryRequest();
        batteryRequest.setName("Sample Battery");
        batteryRequest.setPostCode("12345");
        batteryRequest.setWattCapacity(5000);
        return batteryRequest;
    }

    private BatteryResource buildBatteryResource() {
        BatteryResource batteryResource = new BatteryResource();
        batteryResource.setName("Sample Battery");
        batteryResource.setPostCode("12345");
        batteryResource.setWattCapacity(5000);
        return batteryResource;
    }

    private List<BatteryRequest> buildBatteryRequestList() {
        return List.of(buildBatteryRequest(), buildBatteryRequest());
    }

    private List<BatteryResource> buildBatteryResourcetList() {
        return List.of(buildBatteryResource(), buildBatteryResource());
    }

}