package com.siddhi.virtualpower.battery.repository;

import com.siddhi.virtualpower.battery.domain.Battery;
import com.siddhi.virtualpower.battery.repository.entity.BatteryEntity;
import com.siddhi.virtualpower.battery.repository.mapper.BatteryEntityMapper;
import com.siddhi.virtualpower.battery.repository.mapper.BatteryEntityMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BatteryRepositoryAdaptorTest {

    @Mock
    private BatteryJpaRepository repository;
    private BatteryRepositoryAdaptor adaptor;

    @BeforeEach
    void before(){
        adaptor = new BatteryRepositoryAdaptor(repository, new BatteryEntityMapperImpl());
    }

    @Test
    void givenValidBatteries_whenSaved_shouldReturnValidBattery(){
        var batteries = buildBatteries();
        when(repository.saveAll(any())).thenReturn(buildBatteryEntities());
        var savedBatteries = adaptor.saveAll(batteries);

        Assertions.assertEquals(2,savedBatteries.size());
        Assertions.assertEquals(batteries,savedBatteries);
    }

    @Test
    void givenValidName_whenExistsByName_shouldReturnTrue(){
        var battery = buildBatteries().get(0);
        when(repository.existsByName(battery.getName())).thenReturn(true);
        var result = adaptor.existsByName(battery.getName());

        Assertions.assertEquals(Boolean.TRUE,result);
    }

    @Test
    void givenPostCodes_whenFindByPostCodes_shouldReturnListOfMatchedBatteries(){
        var postCodes = List.of("121","122");

        when(repository.findAllByPostCodeIn(postCodes)).thenReturn(buildBatteryEntities());
        var result = adaptor.findByPostCodes(postCodes);

        List<String> allPostCodes = result.stream()
                .map(Battery::getPostCode)
                .toList();

        Assertions.assertEquals(2,postCodes.size());
        Assertions.assertEquals(postCodes,allPostCodes);
    }

    private List<Battery> buildBatteries() {
        Battery battery1 = new Battery("battery1","121",100);
        Battery battery2 = new Battery("battery2","122",200);

        return List.of(battery1,battery2);
    }

    private List<BatteryEntity> buildBatteryEntities() {
        BatteryEntity batteryEntity1 = new BatteryEntity();
        batteryEntity1.setName("battery1");
        batteryEntity1.setPostCode("121");
        batteryEntity1.setWattCapacity(100);
        BatteryEntity batteryEntity2 = new BatteryEntity();
        batteryEntity2.setName("battery2");
        batteryEntity2.setPostCode("122");
        batteryEntity2.setWattCapacity(200);

        return List.of(batteryEntity1,batteryEntity2);
    }

}