package com.siddhi.virtualpower.battery.service;

import com.siddhi.virtualpower.battery.contract.repository.BatteryRepository;
import com.siddhi.virtualpower.battery.domain.Battery;
import com.siddhi.virtualpower.battery.domain.BatterySummary;
import com.siddhi.virtualpower.battery.exception.InvalidBatteryRequestException;
import com.siddhi.virtualpower.battery.mapper.BatteryMapperImpl;
import com.siddhi.virtualpower.battery.mapper.BatterySummaryMapperImpl;
import com.siddhi.virtualpower.battery.model.BatteryRequest;
import com.siddhi.virtualpower.battery.repository.BatteryQueryRepository;
import com.siddhi.virtualpower.battery.resource.BatteryResource;
import com.siddhi.virtualpower.battery.resource.BatterySearchResource;
import com.siddhi.virtualpower.battery.usecase.GenerateBatterySummaryUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BatteryServiceImplTest {

    @Mock
    private BatteryRepository batteryRepository;
    @Mock
    private BatteryQueryRepository batteryQueryRepository;
    @Mock
    private GenerateBatterySummaryUseCase generateBatterySummaryUseCase;

    private BatteryServiceImpl batteryService;

    @BeforeEach
    public void setup() {
        batteryService = new BatteryServiceImpl(batteryRepository, batteryQueryRepository, generateBatterySummaryUseCase, new BatterySummaryMapperImpl(), new BatteryMapperImpl());
    }

    @Test
    public void givenInvalidBatteryRequests_whenSaveAll_thenThrowException() {
        List<BatteryRequest> batteryRequests = Arrays.asList(
                new BatteryRequest("battery1", "121", 100),
                new BatteryRequest("battery2", "122", 200),
                new BatteryRequest("battery1", "123", 300), // Duplicate name
                new BatteryRequest("battery3", "124", 0),
                new BatteryRequest("battery4", "", 400),
                new BatteryRequest("", "125", 500)
        );

        List<String> expectedErrors = Arrays.asList(
                "Name: battery1 already exists", "Name: battery1 already exists", "Watt is invalid at position 3", "Post Code is missing for battery request at position 4", "Name is missing for battery request at position 5"
        );

        when(batteryService.existsByName("battery1")).thenReturn(true);

        try {
            List<BatteryResource> result = batteryService.saveAll(batteryRequests);
            // If the code reaches here, the test should fail as we expect an exception
            // because of the duplicate name
            fail("Expected InvalidBatteryRequestException, but no exception was thrown.");
        } catch (InvalidBatteryRequestException ex) {
            assertEquals(expectedErrors, ex.getErrors(), "Errors do not match.");
        }

    }

    @Test
    public void givenValidBatteryRequests_whenSaveAll_thenReturnSavedBatteries() {
        List<BatteryRequest> batteryRequests = Arrays.asList(
                new BatteryRequest("battery1", "121", 100),
                new BatteryRequest("battery2", "122", 200)
        );

        List<BatteryResource> expectedResources = Arrays.asList(
                new BatteryResource("battery1", "121", 100),
                new BatteryResource("battery2", "122", 200)
        );

        List<Battery> savedBatteries = Arrays.asList(
                new Battery("battery1", "121", 100),
                new Battery("battery2", "122", 200)
        );

        when(batteryService.existsByName(anyString())).thenReturn(false);
        when(batteryRepository.saveAll(any())).thenReturn(savedBatteries);

        List<BatteryResource> result = batteryService.saveAll(batteryRequests);

        assertEquals(expectedResources, result, "Result resources do not match.");

        // Verify that the repository methods were called as expected
        verify(batteryRepository, times(1)).saveAll(any());
    }

    @Test
    public void givenValidPostCodes_whenSearch_thenReturnBatterySummaryOfPostCodes() {
        List<String> postCodes = Arrays.asList("121", "122");
        List<Battery> matchedBatteries = Arrays.asList(
                new Battery("battery1", "121", 100),
                new Battery("battery2", "122", 200)
        );
        BatterySearchResource expectedResource = BatterySearchResource.builder().batteryNames(List.of("battery1", "battery2")).build();
        when(batteryQueryRepository.findByPostCodes(postCodes)).thenReturn(matchedBatteries);
        when(generateBatterySummaryUseCase.execute(matchedBatteries)).thenReturn(BatterySummary.builder().batteryNames(List.of("battery1", "battery2")).build());

        BatterySearchResource result = batteryService.searchForBatteries(postCodes);

        assertEquals(expectedResource, result, "Search resources do not match.");

        // Verify that the repository methods were called as expected
        verify(batteryQueryRepository, times(1)).findByPostCodes(postCodes);
        verify(generateBatterySummaryUseCase, times(1)).execute(matchedBatteries);
    }

}