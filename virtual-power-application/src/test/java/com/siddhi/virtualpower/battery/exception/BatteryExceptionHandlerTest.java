package com.siddhi.virtualpower.battery.exception;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BatteryExceptionHandlerTest {

    private BatteryExceptionHandler batteryExceptionHandler;

    @BeforeEach
    void setUp() {
        batteryExceptionHandler = new BatteryExceptionHandler();
    }

    @Test
    void testThrowInvalidBatteryRequestException() {
        var errors = List.of("Name: battery1 already exists");
        ResponseEntity<Object> response = batteryExceptionHandler.handleUnProcessableEntityException(
                new InvalidBatteryRequestException(errors, "Invalid Battery Information")
        );

        assertNotNull(response.getBody());
        assertEquals(errors, response.getBody());
    }

}