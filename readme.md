# Virtual Power System

### Introduction
This is an REST API application for virtual power plant system for aggregating distributed power sources into
a single cloud based energy provider.

## Technologies used
* Spring boot 3.1.2
* Java 17
* Lombok
* Liquibase
* Swagger
* Maven

This application follows the Onion Architecture, which is a software architectural pattern that promotes a clear separation of concerns and maintains a strong focus on domain logic. It helps in achieving a scalable, maintainable, and testable codebase.

Liquibase is used for managing database schema changes and versioning, and Swagger provides a user-friendly interface for exploring and testing the API endpoints.

## Running application locally

You can run the application on your local machine by either executing the main method in the class com.siddhi.virtualpower.VirtualPowerApplication from your IDE or navigating to the project folder from the command line and running the following command:

```shell
./mvnw spring-boot:run -pl virtual-power-application -am
```

Once the project is running, the API will be available at the URL http://localhost:8080, and you can find the Swagger documentation of the API at http://localhost:8080/swagger-ui/index.html.

### Notes
* Liquibase is used to initialize the required database tables at application startup, ensuring that the database schema is in sync with the application code.

## Postman collection

The postman collection json file can be found at following location.
```
`postman/Virtual Power.postman_collection.json` 
```
* Import the above file in POSTMAN Client.
* Will find two endpoints being imported.
  1. Bulk Save 
     * To Post Battery Information  
  2. Find By Post Code
     * To get Battery Summary of Post Codes

## APIs
### /api/v1/batteries - POST
* Add collection of batteries 
  * URL for local environment - http://localhost:8080/api/v1/batteries
    * Type="POST"
      * Body:
        ```json
            [
              {
                "name":"batteryName",
                "postCode":"postCode",
                "wattCapacity":1
              }
            ]
        ```

      * Sample Data
        ```json
        [
          {
            "name": "Cannington",
            "postCode": "6107",
            "wattCapacity": 13500
          },
          {
            "name": "Midland",
            "postCode": "6057",
            "wattCapacity": 50500
          },
          {
            "name": "Hay Street",
            "postCode": "6000",
            "wattCapacity": 23500
          },
          {
            "name": "Mount Adams",
            "postCode": "6525",
            "wattCapacity": 12000
          },
          {
            "name": "Koolan Island",
            "postCode": "6733",
            "wattCapacity": 10000
          },
          {
            "name": "Armadale",
            "postCode": "6992",
            "wattCapacity": 25000
          },
          {
            "name": "Lesmurdie",
            "postCode": "6076",
            "wattCapacity": 13500
          },
          {
            "name": "Kalamunda",
            "postCode": "6076",
            "wattCapacity": 13500
          },
          {
            "name": "Carmel",
            "postCode": "6076",
            "wattCapacity": 36000
          },
          {
            "name": "Bentley",
            "postCode": "6102",
            "wattCapacity": 85000
          },
          {
            "name": "Akunda Bay",
            "postCode": "2084",
            "wattCapacity": 13500
          },
          {
            "name": "Werrington County",
            "postCode": "2747",
            "wattCapacity": 13500
          },
          {
            "name": "Bagot",
            "postCode": "0820",
            "wattCapacity": 27000
          },
          {
            "name": "Yirrkala",
            "postCode": "0880",
            "wattCapacity": 13500
          },
          {
            "name": "University of Melbourne",
            "postCode": "3010",
            "wattCapacity": 85000
          },
          {
            "name": "Norfolk Island",
            "postCode": "2899",
            "wattCapacity": 13500
          },
          {
            "name": "Ootha",
            "postCode": "2875",
            "wattCapacity": 13500
          },
          {
            "name": "Kent Town",
            "postCode": "5067",
            "wattCapacity": 13500
          },
          {
            "name": "Northgate Mc",
            "postCode": "9464",
            "wattCapacity": 13500
          },
          {
            "name": "Gold Coast Mc",
            "postCode": "9729",
            "wattCapacity": 50000
          }
        ]
        ```

* The api allows to save batteries. It takes list of batteries in body.


### /api/v1/batteries?postCodes=12345&postCodes=23456 - GET
* List battery summary by Post Codes 
  * URL for local environment - http://localhost:8080/api/v1/batteries?postCodes=12345&postCodes=23456
  * Type="GET"

* The api takes list postCode and returns the summary of batteries associated with the postCodes.